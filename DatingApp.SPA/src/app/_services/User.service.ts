import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { User } from '../_models/User';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AuthHttp } from 'angular2-jwt';
import { PaginatedResult } from '../_models/pagination';

@Injectable()
export class UserService {
  baseUrl = environment.apiUrl;
  // constructor(private http: Http) {}
  constructor(private authHttp: AuthHttp) {}

  getUsers(page?: number, itemsPerPage?: number, userParams?: any, likesParam?: string) {
    const paginatedResult: PaginatedResult<User[]> = new PaginatedResult<User[]>();
    let queryString = '?';
    if (page != null && itemsPerPage != null) {
      queryString += 'pageNumber=' + page + '&pageSize=' + itemsPerPage + '&';
    }

    if (likesParam === 'Likers') {
      queryString += 'Likers=true&';
    }

    if (likesParam === 'Likees') {
      queryString += 'Likees=true&';
    }

    if (userParams != null) {
      queryString +=
        '&minAge=' + userParams.minAge +
        '&maxAge=' + userParams.maxAge +
        '&gender=' + userParams.gender +
        '&orderBy=' + userParams.orderBy;
    }

    return this.authHttp
      .get(this.baseUrl + 'users' + queryString) // .get(this.baseUrl + 'users', this.jwt())
      .map((response: Response) => {
        paginatedResult.result = response.json();
        if (response.headers.get('Pagination') != null) {
          paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
        }
        return paginatedResult;
      })
      .catch(this.handleError);
  }

  getUser(id): Observable<User> {
    return this.authHttp
      .get(this.baseUrl + 'users/' + id)
      .map(response => <User>response.json())
      .catch(this.handleError);
  }

  updateUser(id: number, user: User) {
    return this.authHttp.put(this.baseUrl + 'users/' + id, user).catch(this.handleError);
  }

  setMainPhoto(userId: number, photoId: number) {
    return this.authHttp
      .post(this.baseUrl + 'users/' + userId + '/photos/' + photoId + '/setMain', {})
      .catch(this.handleError);
  }

  deletePhoto(userId: number, photoId: number) {
    return this.authHttp.delete(this.baseUrl + 'users/' + userId + '/photos/' + photoId).catch(this.handleError);
  }

  sendLike(userId: number, recipientId: number) {
    return this.authHttp
      .post(this.baseUrl + 'users/' + userId + '/like/' + recipientId , {})
      .catch(this.handleError);
  }


  // instead of use this method use authHttp from angular2-jwt lib
  // note that for use authHtpp we done configration in auth module
  // angular2-jwt is a small and unopinionated library that is useful for automatically attaching a
  // JSON Web Token (JWT) as an Authorization header when making HTTP requests from an Angular 2 app.
  // It also has a number of helper methods that are useful for doing things like decoding JWTs.
  // https://www.npmjs.com/package/angular2-jwt
  // private jwt() {
  //   let token = localStorage.getItem('token');
  //   if (token) {
  //     let headers = new Headers({ Authorization: 'Bearer ' + token });
  //     headers.append('Content-type', 'application/json');
  //     return new RequestOptions({ headers: headers });
  //   }
  // }

  // extract errors from respond (with a clean meassage)
  private handleError(error: any) {
    if (error.status === 400) {
      return Observable.throw(error._body);
    }
    const applicationError = error.headers.get('Application-Error');
    if (applicationError) {
      return Observable.throw(applicationError);
    }

    const serverError = error.json();
    let modelStatError = '';
    if (serverError) {
      for (const key in serverError) {
        if (serverError[key]) {
          modelStatError += serverError[key] + '\n';
        }
      }
    }
    return Observable.throw(modelStatError || 'server error');
  }
}
