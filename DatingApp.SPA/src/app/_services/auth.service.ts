import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import {tokenNotExpired, JwtHelper} from 'angular2-jwt';
import { User } from '../_models/User';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

// custom service should be imported to app.module and then added in provider

// @Injectable() decorator let us to inject things to a service whils a component is injectable and 
// does not need this decorator
@Injectable()
export class AuthService {
    baesUrl = 'http://localhost:5000/api/auth/';
    userToken: any;
    decodedToken: any;
    currentUser: User;
    jwtHelper: JwtHelper = new JwtHelper();
    private photoUrl = new BehaviorSubject<string>('../../assets/user.png');
    currentPhotoUrl = this.photoUrl.asObservable();

    constructor(private http: Http) { }

    changeMemberPhoto(photoUrl: string) {
        this.photoUrl.next(photoUrl);
    }

    login(model: any) {
        return this.http.post(this.baesUrl + 'login', model, this.requestOptions()).map((response: Response) => {
            const user = response.json();
            if (user) {
                // save the token that has sent from back-end in localStorage when loggin successed
                localStorage.setItem('token', user.tokenString);
                localStorage.setItem('user', JSON.stringify(user.user));
                this.decodedToken = this.jwtHelper.decodeToken(user.tokenString);
                this.userToken = user.tokenString;
                this.currentUser = user.user;
                if  (this.currentUser.photoUrl !== null) {
                    this.changeMemberPhoto(this.currentUser.photoUrl);
                } else {
                    this.changeMemberPhoto('../../assets/user.png');
                }
                // I did next line just for my knowlodge
                localStorage.setItem('decodedToken', JSON.stringify(this.decodedToken));
            }
        }).catch(this.handleError);
    }

    register(user: User) {
        return this.http.post(this.baesUrl + 'register', user, this.requestOptions()).catch(this.handleError);
    }

    loggedIn() {
        return tokenNotExpired();
    }

    private requestOptions() {
        const headers = new Headers({'Content-type': 'application/json'});
        return new RequestOptions({headers: headers});
    }

    // extract errors from respond (with a clean meassage)
    private handleError(error: any) {
        const applicationError = error.headers.get('Application-Error');
        if (applicationError) {
            return Observable.throw(applicationError);
        }

        const serverError = error.json();
        let modelStatError = '';
        if (serverError) {
            for (const key in serverError) {
                if (serverError[key]) {
                    modelStatError += serverError[key] + '\n';
                }
            }
        }
        return Observable.throw(
            modelStatError || 'server error'
        );
    }

}

