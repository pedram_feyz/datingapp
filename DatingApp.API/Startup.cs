﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DatingApp.API.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Http;
using DatingApp.API.Helpers;
using AutoMapper;

namespace DatingApp.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add or inject DataContext to our app
            // and with lambda expresion say it we use Sqlite as DB 
            // and send connection string as a paaram to it that comes from appsetting.json file
            services.AddDbContext<DataContext>(x => x.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<Seed>();
            services.AddCors();
            services.Configure<CloudinarySettings>(Configuration.GetSection("CloudinarySettings"));
            // at first add auto-mapper packge to our app(can be assed by nuget package manager) and then added to our servicses in here
            // note: to use auto-mapper, should make a class that derived form profile class (see AutoMapperProfiles class)
            services.AddAutoMapper();
            // addScope creates a new instance of AuthRepository for each http request
            services.AddScoped<IAuthRepository, AuthRepository>();
            services.AddScoped<IDatingRepository, DatingRepository>();

            // this part is added to use [Authorize] attr in controller
            var key = Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => {
                    options.TokenValidationParameters =new TokenValidationParameters{
                        ValidateIssuerSigningKey =true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
            });

            // ignoring Reference Loop , e.g: user has a refrence to photos and photo has a refrence to a user
            //services.AddMvc();
            services.AddMvc().AddJsonOptions(opt => {
                opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
            services.AddScoped<LogUserActivity>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, Seed seeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // global exception handler
                app.UseExceptionHandler(builder => 
                {
                    builder.Run(async context => {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if(error != null)
                        {
                            // add error to header of respond
                            context.Response.AddApplicationError(error.Error.Message);
                            // add error to body of respond
                            await context.Response.WriteAsync(error.Error.Message);
                        }
                    });
                });
            }

            // this one was used for seeding our DB, for moere info see seed class
            //seeder.SeedUsers();

            // https://www.tutorialspoint.com/rest_api/rest_api_cors_and_enabling_cors.asp
            // issue of different origin => URLs have same origin if they have identical schemes(protocol), host, port
            // in my case SPA app is http://localhost:4200 and API app is http://localhost:5000
            // use this service to solve security issue in browser when we run app fron-end 4200 and back-end 5000
            // for browser is diffrent domains
            // in sumarise this service add some attr to respnse header that comes back from back-end
            app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
