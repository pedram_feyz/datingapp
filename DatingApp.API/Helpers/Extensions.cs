using System;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DatingApp.API.Helpers
{
    public static class Extensions
    {
        //in C#, Extension methods(is a special kind of static method) enable you to add methods to existing types without creating a new derived type
        //This keyword. An extension method uses the this-keyword in its parameter list. It must be located in a static class. We often use special "Extensions" classes.
        // add error to header of response
        public static void AddApplicationError(this HttpResponse response, string messsage)
        {
            response.Headers.Add("Application-Error", messsage);
            response.Headers.Add("Access-Control-Expose-Header", "Application-Error");
            response.Headers.Add("Access-Control-Allow-Origin", "*");
        }

        public static void AddPagination(this HttpResponse response, int currentPage, int itemsPerPage, int totalItems, int totalPages)
        {
            var paginationHeader = new PaginationHeader(currentPage, itemsPerPage, totalItems, totalPages);
            // to send respond back in camel case
            var camelCaseFormatter = new JsonSerializerSettings();
            camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
            // add to header
            response.Headers.Add("Pagination", JsonConvert.SerializeObject(paginationHeader, camelCaseFormatter));
            //  indicates which headers can be exposed as part of the response by listing their names.
            // can be accessable in by response.headers.get('Pagination') in fron-end
            response.Headers.Add("Access-Control-Expose-Headers", "Pagination");
        }

        public static int CalCulateAge(this DateTime theDateTime) 
        {
            var age = DateTime.Today.Year - theDateTime.Year;
            if(theDateTime.AddYears(age) > DateTime.Today)
                age--;

            return age;
        }
    }
}